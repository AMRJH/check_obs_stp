import os
import sys
import time
import datetime
import traceback
from pathlib import Path
from copy import copy, deepcopy
from os.path import dirname, realpath
ObSFi_Error={}
# ----- Support Function -----
def is_num(s):
    # PH
    try:
        float(s)
        return True
    except:
        return False
def is_int(num):
    try:
        int(num)
        return True
    except:
        return False
def extrct_num(tmp,numOnly=False,sign=False,single=False,index=0):
    # Version 4.0
    import re
    num=None
    numList=[]
    try:
        # Number Only
        if numOnly:
            num=re.sub("[^0-9]", "", str(tmp))
            return num
        # Signed Number
        if sign:
            # Remove ','
            tmp = str(tmp).replace(',', '')
            numList = re.findall(r'[-+]?\d*\.\d+|[-+]?\d+', str(tmp))
        # Non signed Number
        if not sign:
            numList = re.findall(r'\d+', str(tmp))
        # Convert To Float
        numList = [float(x) for x in numList]
        # Return Multiple Number
        if not single:
            return numList
        # Return Single Number
        if single:
            # Return Based on Index:
            if index < len(numList):
                num = numList[index]
            return num
    except:
        return None
# ----- Excel Function -----
def opn_worksht(scBk,scshtName):
    # Version 3.0
    try:
        scsht = scBk.Worksheets(scshtName)
        return scsht
    except:
        return None
def cnvrt_col_to_num(scsht, iCChar='A'):
    tmp = scsht.Range(str('${}$1:${}$1'.format(str(iCChar), str(iCChar))))
    return tmp.Column
# ----- OBS<->STP Operation -----
def extrct_SiD(scsht,iRStRt=2,iREnD=None,iC='A'):
    '''
    Version: 1.0
    Used to extract all series ID from target column
    SiD_iR=extrct_SiD(scsht,iRStRt=2,iREnD=None,iC=1)
    '''

    SiD_iR={}
    emptyiR=0
    if iREnD is None:
        iREnD = int(scsht.Range(scsht.UsedRange.Address.split(':')[1]).Row) + 1
    if not is_int(iC):
        iC=cnvrt_col_to_num(scsht, iCChar=iC)
    for iR in range(iRStRt,iREnD):
        SiD=None
        SiD=scsht.Cells(iR,iC).Value
        if SiD is not None:
            emptyiR=0
            SiDNum = None
            SiDNum = extrct_num(SiD, single=True)
            if SiDNum is not None:
                if is_num(SiDNum):
                    if SiDNum not in SiD_iR:
                        SiD_iR.update({SiDNum: iR})
        else:
            emptyiR+=1
        if emptyiR>10:
            break
    return SiD_iR
def extrct_Date(scsht,iR,iCStRt):
    '''
    Version: 1.0
    # Extract Date from Obs @ Stp file
    dateObS_iCObS=extrct_Date(scshtObS,iR=1,iCStRt='N')
    dateStP_iCStP=extrct_Date(scshtStP,iR=1,iCStRt='AA')
    '''
    emptyiC = 0
    date_iC = {}
    date_ColorIndex={}
    dtFormat = "%#m/%#d/%Y"
    iCStRt = cnvrt_col_to_num(scsht, iCChar=iCStRt)
    iCEnD = int(scsht.Range(scsht.UsedRange.Address.split(':')[1]).Column) + 1
    for iC in range(iCStRt, iCEnD):
        dateV = scsht.Cells(iR, iC).Value
        # Extract The Column's Fill
        ColorIndex=scsht.Cells(iR, iC).Interior.ColorIndex
        if dateV is not None:
            emptyiC = 0
            try:
                tmp = dateV.date().strftime(dtFormat)
                date = datetime.datetime.strptime(str(tmp), '%m/%d/%Y')
                date_iC.update({date: iC})
                date_ColorIndex.update({date:ColorIndex})
            except:
                pass
        else:
            emptyiC += 1
        if emptyiC > 5:
            break
    return date_iC,date_ColorIndex
def insrt_NewDate(scshtObS,date_scData,date_iC,date_iC_New,InClrIndx=45):
    '''
    Version 1.0
    date_iC, date_iC_New=insrt_NewDate(scshtObS,date_scData,date_iC,date_iC_New)
    '''
    NtFnDDateL = []
    if len(date_iC) == 0:
        date_iC, date_ColorIndex = extrct_Date(scshtObS, iR=1, iCStRt='N')
    DatePhase_dateL = {}
    DatePhase_iCStRt = {}
    DatePhase_iCEnd = {}
    # New Date
    if len(date_iC) == 0:
        DatePhase_dateL.update({1:sorted(date_scData.keys(),reverse=True)})
        iCStRt = 14
        iCEnd = iCStRt+len(date_scData)-1
        DatePhase_iCStRt.update({1:iCStRt})
        DatePhase_iCEnd.update({1:iCEnd})
    # Update New date:
    else:
        if len(set(date_scData).intersection(date_iC)) != len(date_scData):
            date_iC_New = {}
            # Combine New Date
            dateiCL = [date for date in date_iC]
            dateScDataL = [date for date in date_scData]
            ALDateL = list(set(dateiCL + dateScDataL))
            # Calculate Real Date Column
            NiCL = []
            NtFnDDateL = []
            for i, date in enumerate(sorted(ALDateL, reverse=True), start=14):
                if date not in dateiCL:
                    NiCL.append(i)
                    NtFnDDateL.append(date)
                    date_iC_New.update({date: i})
            # Generate Phase Date (Phasing Algorithms)
            DatePhase_iC = {}
            DatePhase_dateL = {}
            xdiff = [NiCL[n] - NiCL[n - 1] for n in range(0, len(NiCL))]
            if xdiff.count(1) != len(NiCL) - 1:
                phase = 0
                for i, diff in enumerate(xdiff):
                    if diff != 1 and diff > 0:
                        phase += 1
                    if phase not in DatePhase_iC:
                        DatePhase_iC.update({phase: [NiCL[i]]})
                        DatePhase_dateL.update({phase: [NtFnDDateL[i]]})
                    else:
                        DatePhase_iC[phase].append(NiCL[i])
                        DatePhase_dateL[phase].append(NtFnDDateL[i])
            else:
                DatePhase_iC.update({0: NiCL})
                DatePhase_dateL.update({0: NtFnDDateL})
            # Generate Date Phase Range:
            iCL=[]
            DatePhaseL=[]
            DatePhaseL = DatePhase_iC.keys()
            iCL = DatePhase_iC.values()
            iCStRtL = [iC[0] for iC in iCL]
            iCEndL = [iC[-1] for iC in iCL]
            DatePhase_iCStRt = dict(zip(DatePhaseL, iCStRtL))
            DatePhase_iCEnd = dict(zip(DatePhaseL, iCEndL))
    # Write into Observation File
    if len(DatePhase_dateL)>0:
        for phase in DatePhase_dateL:
            dateL=[]
            dateL = [date.date().strftime("%#m/%#d/%Y") for date in DatePhase_dateL[phase]]
            iCStRtADRSC = str(scshtObS.Cells(1, int(DatePhase_iCStRt[phase])).Address).split('$')[1]
            iCEndADRSC = str(scshtObS.Cells(1, int(DatePhase_iCEnd[phase])).Address).split('$')[1]
            iCRng = "{}1:{}1".format(str(iCStRtADRSC), str(iCEndADRSC))
            iCRngB = "{}:{}".format(str(iCStRtADRSC), str(iCEndADRSC))
            scshtObS.Range(iCRngB).Insert()
            if str(scshtObS.Name).lower().find("d") > -1 or str(scshtObS.Name).lower().find("w") > -1:
                scshtObS.Cells.Range(iCRng).NumberFormat = "dd-mmm-yyyy"
                scshtObS.Range(iCRng).Value = dateL
                scshtObS.Range(iCRng).Interior.ColorIndex = InClrIndx
            else:
                scshtObS.Cells.Range(iCRng).NumberFormat = "mmm-yyyy"
                scshtObS.Range(iCRng).Value = dateL
                scshtObS.Range(iCRng).Interior.ColorIndex = InClrIndx
            scshtObS.Columns(iCRngB).EntireColumn.AutoFit()
    return NtFnDDateL
def validate_Date(scshtObS,dateStP_iCStP,dateObS_iCObS):
    '''
    Version: 1.0
    Date Created: 02/04/2020
    Date Updated: 02/04/2020
    Used to validate_OBS_STP the between Observation & STP
    <> Support
        - Auto insert not found date within range
    <> Usage
    validate_Date(scshtObS,dateStP_iCStP,dateObS_iCObS)
    '''
    NtFnDDateL=[]
    try:
        date_iC1 = {}
        date_iC2 = {}
        if len(dateObS_iCObS) > len(dateStP_iCStP):
            date_iC1 = deepcopy(dateStP_iCStP)
            date_iC2 = deepcopy(dateObS_iCObS)
        else:
            date_iC1 = deepcopy(dateObS_iCObS)
            date_iC2 = deepcopy(dateStP_iCStP)
        matchDate = []


        for date in date_iC1:
            if date in date_iC2:
                matchDate.append(date)
        # Get the latest matched Date:
        ltstStPDate = list(dateStP_iCStP.keys())[0]
        if len(matchDate)>0:
            ltstMcthDate = matchDate[0]
            oldMcthDate = matchDate[-1]
            # Get the date before latest matched Date in StP:
            validDateStP = []
            for date in sorted(dateStP_iCStP.keys(), reverse=False):
                if date >= oldMcthDate:
                    validDateStP.append(date)
            # All Date in Obs must same with validDateStP
            notFoundObsDate = []
            for date in sorted(validDateStP, reverse=False):
                if date not in dateObS_iCObS:
                    notFoundObsDate.append(date)
            # Added New Date in Obs File:
            if len(notFoundObsDate) > 0:
                date_iC = {}
                date_iC_New = {}
                date_scData = dict(zip(notFoundObsDate, list(range(0, len(notFoundObsDate)))))
                NtFnDDateL= insrt_NewDate(scshtObS=scshtObS
                                         , date_scData=date_scData
                                         , date_iC=dateObS_iCObS
                                         ,date_iC_New=dateObS_iCObS)
        else:
            print("Date In Observation file not match with STP")
    except:
        print(traceback.format_exc())
    return NtFnDDateL
def extrct_Marked_Date(date_ColorIndex,TColorIndex=None):
    '''
    Version 1.0
    Date Created: 02/05/2020
    Date Updated: 02/05/2020
    Used to extract date based only on specific cell color fill
    '''
    dateL=[]
    if TColorIndex is None:
        TColorIndex=6
    for date,ColorIndex in date_ColorIndex.items():
        if int(ColorIndex)==int(TColorIndex):
            dateL.append(date)
    return dateL
def insrt_STPDate_ToObs(scshtObS,scshtStP,SiDObS_iRObS,SiDStP_iRStP,dateStP_iCStP):
    # Date Column Start in Observation File:
    iCObS=cnvrt_col_to_num(scshtObS,'N')

    # Insert New STP date To Observation File:
    dateObS_iCObS={}
    date_iC_New={}
    # Extract STP Date and scData:
    dateStP_scDataStP={}
    dateStP_StPDate_scDataStP={}
    StPDateCount=0
    for dateStP,iCStP in dateStP_iCStP.items():
        if StPDateCount<2:
            SiDStP_scDataStP = {}
            for SiDStP, iRStP in SiDStP_iRStP.items():
                scDataStP = scshtStP.Cells(iRStP, iCStP).Value
                if scDataStP is not None:
                    SiDStP_scDataStP.update({SiDStP:scDataStP})
            if len(SiDStP_scDataStP)>0:
                dateStP_scDataStP.update({dateStP:0})
                dateStP_StPDate_scDataStP.update({dateStP:SiDStP_scDataStP})
                StPDateCount+=1

    # Insert Valid Date into Observation File:
    insrt_NewDate(scshtObS,date_scData=dateStP_scDataStP,date_iC=dateObS_iCObS,date_iC_New=date_iC_New,InClrIndx=35)
    dateObS_iCObS, date_ColorIndex = extrct_Date(scshtObS, iR=1, iCStRt='N')

    # Insert StPScData in Observation File Based on ID:
    for SiDObS,iRObS in SiDObS_iRObS.items():
        if SiDObS in SiDStP_iRStP:
            for dateObS,iCObS in dateObS_iCObS.items():
                if SiDObS in dateStP_StPDate_scDataStP[dateObS]:
                    scDataStP=dateStP_StPDate_scDataStP[dateObS][SiDObS]
                    scshtObS.Cells(iRObS,iCObS).Value=scDataStP
def validate_OBS_STP(ObSFiPth,StPFiPth,SvERsLtFoPtH):
    '''
    Version 1.0
    Used to perform validation process
    <>Support
        - Check on specific date by highlight it to yellow
        - Auto save Result to target observation's folder
    '''
    # Open ObS
    import win32com.client as xlx
    StP = xlx.gencache.EnsureDispatch("Excel.Application")
    scBkStP = StP.Workbooks.Open(StPFiPth, ReadOnly=True)
    StP.Visible = True
    scshtObS = None

    # Open StP
    import win32com.client as xlz
    ObS = xlz.gencache.EnsureDispatch("Excel.Application")
    scBkObS = ObS.Workbooks.Open(ObSFiPth, ReadOnly=True)
    ObS.Visible = True
    scshtStP = None

    StPShetName=[list_sht.Name for list_sht in scBkStP.Sheets]
    for list_sht in scBkObS.Sheets:
        if list_sht.Name in StPShetName:
            scshtObS = scBkObS.Worksheets(list_sht.Name)
            scshtStP = scBkStP.Worksheets(list_sht.Name)
            scshtStP.Activate() # Show Current Worksheet
            scshtObS.Activate() # Show Current Worksheet
            if scshtObS is not None and scshtStP is not None:
                # Stage 1: Date Validation
                SiDObS_iRObS = extrct_SiD(scshtObS, iRStRt=2, iREnD=None, iC='A')
                SiDStP_iRStP = extrct_SiD(scshtStP, iRStRt=2, iREnD=None, iC='A')
                dateObS_iCObS, date_ColorIndex = extrct_Date(scshtObS, iR=1, iCStRt='N')
                dateStP_iCStP, date_ColorIndex = extrct_Date(scshtStP, iR=2, iCStRt='AA')
                # Validate if Observation File's Date Exist
                if len(dateObS_iCObS)>0:
                    NtFnDDateL=validate_Date(scshtObS, dateStP_iCStP, dateObS_iCObS)
                    dateObS_iCObS, date_ColorIndex = extrct_Date(scshtObS, iR=1, iCStRt='N')
                    dateL = []
                    dateL = extrct_Marked_Date(date_ColorIndex, TColorIndex=6)
                    if len(dateL)==0:
                        dateL=list(dateObS_iCObS.keys())
                    else:
                        dateL = dateL + NtFnDDateL
                    # Stage 2: Validate ScData
                    validate_scData(scBkObS, scshtObS, scshtStP, SiDObS_iRObS, SiDStP_iRStP, dateObS_iCObS, dateStP_iCStP,dateL)
                # Consider To Insert STP's Value To Observation:
                if len(dateObS_iCObS)==0:
                    insrt_STPDate_ToObs(scshtObS, scshtStP, SiDObS_iRObS, SiDStP_iRStP, dateStP_iCStP)
    # Stage 3: Saving Result File:
    # Auto Save Result in Observation Folder:
    if SvERsLtFoPtH is None:
        SvERsLtFoPtH = Path(ObSFiPth).parent.absolute()
    RESULTF = create_Fol(SvERsLtFoPtH, 'Result')
    resultFname = "{}\R{}".format(str(RESULTF), str(scBkObS.Name))
    if os.path.exists(resultFname):
        # Renaming Old Observation File_
        currTime = datetime.datetime.utcnow().strftime("%Y%m%d%H%M%S")
        oldVersion = "{}\{}_{}".format(str(RESULTF), str(currTime), str(os.path.basename(resultFname)))
        os.rename(resultFname, oldVersion)
    scBkObS.SaveAs(resultFname)
    # Force Close Observation File:
    try:
        scBkObS.Close(SaveChanges=False)
    except:
        pass
    # Force StP File:
    try:
        scBkStP.Close(SaveChanges=False)
    except:
        pass
def validate_scData(scBkObS,scshtObS,scshtStP,SiDObS_iRObS,SiDStP_iRStP,dateObS_iCObS,dateStP_iCStP,dateL):
    '''
    Version 2.0
    Compare OBS data with STP data
    validate_scData(scBkObS, scshtObS, scshtStP, SiDObS_iRObS, SiDStP_iRStP, dateObS_iCObS, dateStP_iCStP, dateL)
    '''
    # scData Validation
    TtLiR = len(SiDObS_iRObS)
    iR = 0
    PrGStSts=None
    for SiDObS, iRObS in SiDObS_iRObS.items():
        if SiDObS in SiDStP_iRStP:
            iRStP = SiDStP_iRStP[SiDObS]
            # Looping For Date:
            if len(dateL) == 0:
                dateL = list(dateObS_iCObS.keys())
                if len(dateObS_iCObS) > len(dateStP_iCStP):
                    dateL = list(dateStP_iCStP.keys())
            for date in dateL:
                if date in dateStP_iCStP and date in dateObS_iCObS:
                    iCObS = dateObS_iCObS[date]
                    iCStP = dateStP_iCStP[date]
                    scDataObS = scDataStP = None
                    scDataObS = scshtObS.Cells(iRObS, iCObS).Value
                    scDataStP = scshtStP.Cells(iRStP, iCStP).Value
                    scshtObS.Cells(iRObS, 14).Activate()  # Show Current Cells
                    # scshtStP.Cells(iRStP, iCStP).Activate()  # Show Current Cells
                    # Display Progress
                    sys.stdout.write('\r')
                    dat = "[{}] Worksheet: '{}' Progress {}%". \
                        format(str(scBkObS.Name), str(scshtObS.Name), str(round(((iR) / (TtLiR)) * 100, 1)))
                    PrGStSts=dat
                    sys.stdout.write(dat)
                    sys.stdout.flush()
                    # Checking Process:
                    i=0
                    MtCStS=False
                    FntClR=CeLClR=CelVal=None
                    if scDataObS is not None and scDataStP is not None:
                        if is_num(scDataObS) and is_num(scDataStP):
                            for i in range(0,3):
                                MtCStS, FntClR, CeLClR=match_T1(scDataObS, scDataStP)
                                if i !=0:
                                    CelVal="{} StP={}".format(str(scDataObS), str(scDataStP))
                                if i== 1:
                                    MtCStS, FntClR, CeLClR = match_T2(scDataObS, scDataStP)
                                # Edit your own Rule Here:
                                # if i == 2:
                                #     MtCStS, FntClR, CeLClR = match_T2(scDataObS, scDataStP)
                                # if i==3:
                                #     MtCStS, FntClR, CeLClR = match_T3(scDataObS, scDataStP)
                                # if i==4:
                                #     MtCStS, FntClR, CeLClR = match_T4(scDataObS, scDataStP)
                                if MtCStS:
                                    break
                            # Not Match with STP
                            if not MtCStS:
                                FntClR = 1  # Black
                                CeLClR = 44  # Light Orange Fill
                    if not is_num(scDataObS) and is_num(scDataStP):
                        FntClR = 1  # Black
                        CeLClR = 44  # Light Orange Fill
                        scshtObS.Cells(iRObS, iCObS).Value = scDataStP
                    # No Data Exist in Obs
                    if scDataObS is None and scDataStP is not None:
                        FntClR=1 # Black
                        CeLClR= 44  # Light Orange Fill
                        scshtObS.Cells(iRObS, iCObS).Value = scDataStP
                    if scDataStP is None and scDataObS is not None:
                        FntClR=1 # Black
                        CeLClR = 36  # Light Yellow Fill
                        CelVal="{} StP= None ".format(str(scDataObS))
                    # Write Result To Obs:
                    if FntClR is not None and CeLClR is not None:
                        scshtObS.Cells(iRObS, iCObS).Font.ColorIndex = FntClR
                        scshtObS.Cells(iRObS, iCObS).Interior.ColorIndex = CeLClR
                        if i>0:
                            scshtObS.Cells(iRObS, iCObS).Value = CelVal
        iR += 1
    print('')
# ---- File&Folder Operation -----
def create_Fol(savePath,foldrName):
    '''
    Create a new Folder
    FoPth=create_Fol(savePath,foldrName)
    '''
    trgtFldrPath = "{}\{}".format(str(savePath), str(foldrName))
    if not os.path.exists(trgtFldrPath):
        os.mkdir(trgtFldrPath)
    return trgtFldrPath
def ObSFiStPFo(ObSin,StPin,SvERsLtFoPtH):
    StPID_StPFiPth={}
    StPID_StPFiPth=extrct_StPFiPth(StPin)
    if len(StPID_StPFiPth)>0:
        ObSFiNm=os.path.basename(ObSin)
        ObSID=None
        ObSID=extrct_num(ObSFiNm,single=True)
        if is_int(ObSID):
            if int(ObSID) in StPID_StPFiPth:
                validate_OBS_STP(ObSin, StPID_StPFiPth[int(ObSID)],SvERsLtFoPtH)
            else:
                ObSFi=ObSFiNm
                Error = "No Matched StP Found in {}".format(str(StPin))
                if ObSFi not in ObSFi_Error:
                    ObSFi_Error.update({ObSFi: [Error]})
                else:
                    ObSFi_Error[ObSFi].append(Error)
def ObSFoStPFo(ObSin,StPin,SvERsLtFoPtH):
    # Extract StP File Path
    StPID_StPFiPth = {}
    StPID_StPFiPth = extrct_StPFiPth(StPin)
    for ObSFi in os.listdir(ObSin):
        if str(ObSFi).find('.xlsx')>=0:
            ObSFiPth="{}\{}".format(str(ObSin),str(ObSFi))
            ObSID = None
            ObSID = extrct_num(ObSFi, single=True)
            if is_int(ObSID):
                if int(ObSID) in StPID_StPFiPth:
                    try:
                        validate_OBS_STP(ObSFiPth, StPID_StPFiPth[int(ObSID)],SvERsLtFoPtH)
                    except:
                        pass
                else:
                    Error="No Matched StP Found in {}".format(str(StPin))
                    if ObSFi not in ObSFi_Error:
                        ObSFi_Error.update({ObSFi:[Error]})
                    else:
                        ObSFi_Error[ObSFi].append(Error)
        else:
            Error="Not Valid Observation File"
            if ObSFi not in ObSFi_Error:
                ObSFi_Error.update({ObSFi: [Error]})
            else:
                ObSFi_Error[ObSFi].append(Error)
def extrct_StPFiPth(StPin):
    # TODO : Row & Column Limit
    # Checking StP folder:
    import pathlib
    import xlwings.constants
    import win32com.client as xlx
    StPFo = StPin
    StPID_StPFiPth = {}
    for StPFi in os.listdir(StPFo):
        StPFiPth = None
        StPFiPth = "{}\{}".format(str(StPFo), str(StPFi))
        if str(StPFi).find('.xlsx') >= 0:
            StPFiName = str(StPFi).split('.xlsx')[0]
            if not is_num(StPFiName):
                # ------- Open StP File -------
                ws = 1
                sf = xlx.gencache.EnsureDispatch("Excel.Application")
                scBk = sf.Workbooks.Open(StPFiPth, ReadOnly=True)
                sf.Visible = True
                scsht = scBk.Worksheets(ws)
                # ----- End StP Source File -----
                PID = None
                fndPID = scsht.Cells.Find(What="Source: ["
                                          , SearchOrder=xlwings.constants.SearchOrder.xlByColumns
                                          , LookAt=xlwings.constants.LookAt.xlPart
                                          , MatchCase=False)
                if fndPID is not None:
                    if str(fndPID).find(']') >= 0:
                        tmp = fndPID
                        tmpSlice = str(tmp)[str(tmp).find('['):str(tmp).find(']')]
                        PID = extrct_num(tmpSlice, single=True)
                        if is_int(PID):
                            PID = int(PID)
                        else:
                            PID = None
                # Force Close Excel
                try:
                    scBk.Close(SaveChanges=False)
                except Exception as e:
                    str(e)
                # Rename The File:
                if PID is not None:
                    PID = int(PID)
                    fileFormat = pathlib.Path(StPFiPth).suffix
                    StPFiRnme = "{}\{}".format(str(StPFo), str(str(PID) + str(fileFormat)))
                    if not os.path.exists(StPFiRnme):
                        print("Renaming {} to {}".format(str(StPFiName),str(PID)))
                        os.rename(StPFiPth, StPFiRnme)
                    else:
                        print("Warning: Rename Failed.. '{}' already exist as '{}'".format(str(StPFi),str(str(PID) + str(fileFormat))))
                    if PID not in StPID_StPFiPth:
                        StPID_StPFiPth.update({PID: StPFiRnme})
            else:
                PID = int(StPFiName)
                if PID not in StPID_StPFiPth:
                    StPID_StPFiPth.update({PID: StPFiPth})
    return StPID_StPFiPth
# ----- Match Rules -----
def match_T1(scDataObS,scDataStP):
    '''
    STP:123 == OBS:123?
    '''
    CeLClR = 0 # No Color
    FntClR = 23  # Blue Text
    MtCStS=False
    if is_num(scDataObS) and is_num(scDataStP):
        if float(scDataObS)-float(scDataStP)== 0:
            MtCStS=True
    return MtCStS,FntClR,CeLClR
def match_T2(scDataObS,scDataStP):
    '''
    Rounding
    OBS:123.456 STP:123.5
    '''
    MtCStS = False
    FntClR = 0  # Black Text
    CeLClR = 20  # Light blue Fill
    try:
        # Remove .
        if str(scDataStP).find('.') > -1 and str(scDataObS).find('.') > -1:
            scDataStP=str(scDataStP).replace('.','')
            scDataObS=str(scDataObS).replace('.','')
        scData1=scDataStP
        scData2=scDataObS
        if len(str(scDataStP))>len(str(scDataObS)):
            scData1=scDataObS
            scData2=scDataStP
        LstStPV = str(scData1)[-1]
        if float(str(scData1)[:-1]) != 0:
            scData1 = str(scData1)[:-1]
        if str(scData2).find(str(scData1)) > -1:
            MtCStS = True
    except:
        print(traceback.format_exc())
    return MtCStS, FntClR, CeLClR
def match_T3(scDataObS,scDataStP):
    '''
    STP:123.456 <> OBS: 123456
    '''
    MtCStS=False
    FntClR = 0  # Black Text
    CeLClR = 36  # Light yellow Fill
    # Remove .
    if str(scDataStP).find('.')>-1:
        scDataStP=str(scDataStP).replace('.','')
        # Remove last STP value:
        scDataStP=str(scDataStP)[:-1]
    if str(scDataObS).find('.')>-1:
        scDataObS=str(scDataObS).replace('.','')
    # Find STP in OBS
    # STP: 12345 <> OBS: 123456 ?
    if str(scDataObS).find(scDataStP)>-1:
        MtCStS = True
    return MtCStS, FntClR, CeLClR
# ----- Main Function -----
def main(ObSin,StPin):
    # [ObS.xlsx]<>[StP.xlsx]
    # Folder Path To Save Result File
    # filepath = realpath(__file__)
    # SvERsLtFoPtH = dirname(filepath)
    SvERsLtFoPtH=None
    if os.path.isfile(ObSin) and os.path.isfile(StPin):
        # validate_stp_obs(ObSin, StPin,SvERsLtFoPtH)
        validate_OBS_STP(ObSin, StPin,SvERsLtFoPtH)
    # [ObS.xlsx]<>[StP Folder]
    if os.path.isfile(ObSin) and os.path.isdir(StPin):
        ObSFiStPFo(ObSin, StPin,SvERsLtFoPtH)
    # [ObS Folder]<>[StP Folder]
    if os.path.isdir(ObSin) and os.path.isdir(StPin):
        ObSFoStPFo(ObSin, StPin,SvERsLtFoPtH)
    # Error Log
    if len(ObSFi_Error) > 0:
        print('\n ---- Error Log ----')
        for ObSFi in ObSFi_Error:
            print("[{}]: ".format(str(ObSFi)))
            for Error in ObSFi_Error[ObSFi]:
                print("\tError: {}".format(str(Error)))
StPin = sys.argv[1] # Insert Your STP File @ Folder Path
ObSin = input("Observation File/Folder : ")
if str(ObSin).find('"')>-1:
    ObSin=str(ObSin).replace('"',"")
main(ObSin,StPin)