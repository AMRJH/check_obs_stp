﻿###################################################################

# initialize variable

###################################################################
$computername = hostname
$temp =  Get-ChildItem -path Env:\USERNAME
$userlogin = $temp.Value

$emailfrom = "businesstech@ceicdata.com"
$smtp = "ceicdata-com.mail.protection.outlook.com"
$businesstechemail = @("$($userlogin)@isimarkets.com")

#### read setup.txt in current folder @ parent folder
$currentfolder = "$PSScriptRoot"
$setupfile = "$($currentfolder)\setup.txt"
if (!(Test-Path -Path "$($setupfile)")) {
    $setupfile = "$(split-path -Parent $currentfolder)\setup.txt"

    if (Test-Path -Path "$($setupfile)") {
        $AWSODpath = Get-Content -Path "$($setupfile)" | Select -Index 0 -ErrorAction SilentlyContinue
        $onedrivepath = Get-Content -Path "$($setupfile)" | Select -Index 1 -ErrorAction SilentlyContinue

    }else {
        $subject = "[Automation Alert] File Setup.txt not found. Machine: " + $computername +". Account: " + $userlogin
        Send-MailMessage -From $emailfrom -To $businesstechemail -Subject $subject -SmtpServer $smtp
        Exit
    }
}else {
    $AWSODpath = Get-Content -Path "$($setupfile)" | Select -Index 0 -ErrorAction SilentlyContinue
    $onedrivepath = Get-Content -Path "$($setupfile)" | Select -Index 1 -ErrorAction SilentlyContinue
}

if (!(Test-Path "$($AWSODpath)\")) {
    $subject = "[Automation Alert] $($AWSODpath) not found. Machine: " + $computername +". Account: " + $userlogin
    Send-MailMessage -From $emailfrom -To $businesstechemail -Subject $subject -SmtpServer $smtp
    Exit
}

if (!(Test-Path "$($onedrivepath)\")) {
    $subject = "[Automation Alert] $($onedrivepath) not found. Machine: " + $computername +". Account: " + $userlogin
    Send-MailMessage -From $emailfrom -To $businesstechemail -Subject $subject -SmtpServer $smtp
    Exit
}

$filecountrylist = "$($AWSODpath)\Countrylist(Core).txt"
if (!(Test-Path -path "$($filecountrylist)")){
    $subject = "[Automation Alert] File Countrylist(Core).txt not found. Machine: " + $computername +". Account: " + $userlogin
    $body = "Machine: " + $computername +"`nAccount: " + $userlogin
    Send-MailMessage -From $emailfrom -To $businesstechemail -Subject $subject -SmtpServer $smtp -Body $body
    Exit
}

$countrylist = @()
foreach($line in get-content $filecountrylist){
    $countrylist += "$($line)"

}

<#
$countrylist = @()
$countrylist += "JPN"
$countrylist += "PHL"
$countrylist += "SGP"
$countrylist += "MAC"
$countrylist += "THA"
$countrylist += "NZL"
#>

#$rootpath = "X:\Macros\New Automation\"

#$onedrivepath = "C:\Users\nzulkepli\Internet Securities, LLC\Ng, Ping Tarng (CEIC EMIS) - New Automation\"
# for example for Amirul, replace above with:
# $onedrivepath = "C:\Users\mhazimin\Internet Securities, LLC\Ng, Ping Tarng (CEIC EMIS) - New Automation\"

$newfilelist = @()
$rootlocal = "$($PSScriptRoot)"
for ($i=0; $i -lt $countrylist.Length; $i++) {
    #Set-Location $rootlocal
    $rootpath = "X:\Macros\New Automation\"
    $country = $countrylist[$i]
    if ($($country) -like "*|*"){
        #country using Bitbucket
        $country = $country.split("|")[0] 
        $country = $country.Trim()
        $result = & "$($rootlocal)\NewautoBB(Core).ps1" "$($AWSODpath)\" "$($rootlocal)\" $country
        Start-Sleep -Seconds 20
        Set-Location $rootlocal
        $rootpath = "$($rootlocal)\"
        
    }
   
    #$countrypath = $rootpath + $country + "\"
    

    <#
    # find in macro
    $macropath = $rootpath + $country + "\Macro\"
    
    if (Test-Path $macropath) {
        $filelist = Get-ChildItem -File -Path ($macropath) -Include "*-*-*.xlsx" -Recurse |  Select-Object Name,DirectoryName
    }
    #>

    $filelist = @()
    $macropath = $rootpath + $country + "\Python\"

    if (Test-Path $macropath) {
        $filelist += Get-ChildItem -File -Path ($macropath) -Include "*-*-*.xlsx" -Recurse |  Select-Object Name,DirectoryName
    }


    for ($j=0; $j -lt $filelist.Length; $j++) {
        $file = $filelist[$j]
        if (($file.name -match '-+[a-zA-Z]+-+') -and ($file.directoryname.tolower() -notlike "*save source*") -and ($file.directoryname.tolower() -notlike "*backup*") -and ($file.name -notlike "*backup*")) {
            
            $file | Add-Member -MemberType NoteProperty -Name "SourceID" -Value $temp

            $countryname = $file.name.Substring(0,$file.name.indexof('-'))
            $sourcename = $file.name.Substring($file.name.indexof('-')+1, ($file.name.lastindexof('-')-$file.name.indexof('-')-1))

            $temp = $file.name
            $temp = $temp.Substring($temp.LastIndexOf('-')+1)
            $temp = $temp.Substring(0, $temp.LastIndexOf('.'))
            $sourceid = $temp
            $publication = $file.directoryname
            $publication = $publication.Substring($publication.LastIndexOf('\')+1)

            $obj = New-Object -TypeName psobject
            $obj | Add-Member -MemberType NoteProperty -Name "Country" -Value $countryname
            $obj | Add-Member -MemberType NoteProperty -Name "Source" -Value $sourcename
            $obj | Add-Member -MemberType NoteProperty -Name "Publication" -Value $publication
            $obj | Add-Member -MemberType NoteProperty -Name "Source ID" -Value $sourceid
            $obj | Add-Member -MemberType NoteProperty -Name "Status" -Value ''
            $obj | Add-Member -MemberType NoteProperty -Name "Remark" -Value ''

            $newfilelist += $obj
        }
    }
    #no need to removed folder for those country that using bitbucket after done coz it will taking long time to sync all the remove and creating file+folder
}
$newfilelist | Export-Csv -Path "$($AWSODpath)\MasterList_Core.csv" -ErrorAction SilentlyContinue -NoTypeInformation
$newfilelist | Export-Csv -Path "$($onedrivepath)\MasterList_Core.csv" -ErrorAction SilentlyContinue -NoTypeInformation
